package com.voyagegames.androidjukebox;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.voyagegames.core.android.audio.Artist;

public class ArtistAdapter extends ArrayAdapter<Artist> {
	
	private final Activity mActivity;

	public ArtistAdapter(final Context context, final int resource, final int textViewResourceId, final List<Artist> objects) {
		super(context, resource, textViewResourceId, objects);
		mActivity = (Activity)context;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View row = mActivity.getLayoutInflater().inflate(R.layout.fragment_artist_list, parent, false);
		final TextView name = (TextView)row.findViewById(R.id.artist_list_name);
		
		name.setText(this.getItem(position).name);
		return row;
	}

}
