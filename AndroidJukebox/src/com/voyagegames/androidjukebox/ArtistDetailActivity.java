package com.voyagegames.androidjukebox;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.voyagegames.androidjukebox.data.Content;
import com.voyagegames.core.android.audio.Song;
import com.voyagegames.core.android.media.MediaPlayerHelper;

public class ArtistDetailActivity
		extends FragmentActivity
		implements ArtistDetailFragment.Callbacks {
	
	private String mArtistID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_artist_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
        	final Bundle arguments = new Bundle();
            final ArtistDetailFragment fragment = new ArtistDetailFragment();
            
            mArtistID = getIntent().getStringExtra(ArtistDetailFragment.ARG_ITEM_ID);
            arguments.putString(ArtistDetailFragment.ARG_ITEM_ID, mArtistID);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.artist_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, ArtistListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    
	MediaPlayerHelper mp;

	@Override
	public void onItemSelected(final Song song) {
		Content.addItem(song);
		
		final Intent jukeboxIntent = new Intent(ItemListFragment.NotifyReceiver.class.getName());
		jukeboxIntent.putExtra(Content.FRAGMENT_NUM, Content.FragmentIndex.ITEM_LIST.ordinal());
		sendBroadcast(jukeboxIntent);
		
		final Intent artistIntent = new Intent(ArtistListFragment.NotifyReceiver.class.getName());
		artistIntent.putExtra(Content.FRAGMENT_NUM, Content.FragmentIndex.ARTIST_LIST.ordinal());
		sendBroadcast(artistIntent);
    	
		finish();
	}
	
}
