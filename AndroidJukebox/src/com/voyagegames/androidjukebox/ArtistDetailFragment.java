package com.voyagegames.androidjukebox;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.voyagegames.core.android.audio.Song;
import com.voyagegames.core.android.audio.SongPicker;

public class ArtistDetailFragment extends Fragment {

    public interface Callbacks {

        public void onItemSelected(Song song);
        
    }

    public static final String ARG_ITEM_ID = "item_id";

    private String mArtistID;
    private Callbacks mCallback;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (Callbacks) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + Callbacks.class.getName());
        }
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (getArguments().containsKey(ARG_ITEM_ID)) {
        	mArtistID = getArguments().getString(ARG_ITEM_ID);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_artist_detail, container, false);
        
        final SongPicker sp = new SongPicker();
        final List<Song> songs = sp.getByArtistID(getActivity(), mArtistID);
        
        final ListView listView = (ListView)rootView.findViewById(R.id.artist_detail_list);
        listView.setAdapter(new SongAdapter(getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                songs));
        
        listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				mCallback.onItemSelected(songs.get(position));
			}
        	
        });
        
        return rootView;
    }

}
