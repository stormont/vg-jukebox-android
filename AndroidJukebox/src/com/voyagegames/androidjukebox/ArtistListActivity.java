package com.voyagegames.androidjukebox;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class ArtistListActivity
		extends FragmentActivity
		implements ArtistListFragment.Callbacks {

    private boolean mTwoPane;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_list);

        if (findViewById(R.id.artist_detail_container) != null) {
            mTwoPane = true;
            ((ArtistListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.artist_list))
                    .setActivateOnItemClick(true);
        }
    }

    @Override
    public void onItemSelected(final String id) {
        if (mTwoPane) {
            final Bundle arguments = new Bundle();
            final ArtistDetailFragment fragment = new ArtistDetailFragment();
            
            arguments.putString(ArtistDetailFragment.ARG_ITEM_ID, id);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.artist_detail_container, fragment)
                    .commit();
        } else {
        	final Intent detailIntent = new Intent(this, ArtistDetailActivity.class);
            detailIntent.putExtra(ArtistDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }
}
