package com.voyagegames.androidjukebox;

import com.voyagegames.androidjukebox.data.Content;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class ItemDetailActivity
		extends FragmentActivity
		implements ItemDetailFragment.OnRankingChangedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_item_detail);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
        	final Bundle arguments = new Bundle();
            final ItemDetailFragment fragment = new ItemDetailFragment();
            
            arguments.putString(ItemDetailFragment.ARG_ITEM_ID, getIntent().getStringExtra(ItemDetailFragment.ARG_ITEM_ID));
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.item_detail_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            NavUtils.navigateUpTo(this, new Intent(this, ItemListActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

	@Override
	public void onRankingChanged() {
		final Intent intent = new Intent(ItemListFragment.NotifyReceiver.class.getName());
		intent.putExtra(Content.FRAGMENT_NUM, Content.FragmentIndex.ITEM_LIST.ordinal());
		sendBroadcast(intent);
	}
}
