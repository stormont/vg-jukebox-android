package com.voyagegames.androidjukebox;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.voyagegames.androidjukebox.data.Content;
import com.voyagegames.androidjukebox.data.Content.JukeboxContent;

public class ItemDetailFragment extends Fragment {

    public interface OnRankingChangedListener {

        public void onRankingChanged();
        
    }

    public static final String ARG_ITEM_ID = "item_id";

    private JukeboxContent mItem;
    private OnRankingChangedListener mCallback;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnRankingChangedListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + OnRankingChangedListener.class.getName());
        }
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (getArguments().containsKey(ARG_ITEM_ID)) {
            mItem = Content.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_item_detail, container, false);
        
        if (mItem == null) {
        	return rootView;
        }
    	
        ((TextView) rootView.findViewById(R.id.item_detail_artist)).setText(mItem.data.artist);
        ((TextView) rootView.findViewById(R.id.item_detail_title)).setText(mItem.data.title);
		setScore(rootView);
		
		final ImageButton upVote = (ImageButton)rootView.findViewById(R.id.item_detail_upvote);
		final ImageButton downVote = (ImageButton)rootView.findViewById(R.id.item_detail_downvote);
		
		upVote.setImageResource(android.R.drawable.arrow_up_float);
		upVote.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View arg0) {
				Content.upVote(mItem);
				setScore(rootView);
				mCallback.onRankingChanged();
			}
			
		});

		downVote.setImageResource(android.R.drawable.arrow_down_float);
		downVote.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View arg0) {
				Content.downVote(mItem);
				setScore(rootView);
				mCallback.onRankingChanged();
			}
			
		});
        
        return rootView;
    }
    
    private void setScore(final View rootView) {
    	final TextView ranking = (TextView)rootView.findViewById(R.id.item_detail_ranking);
    	final int score = mItem.ranking.score();
    	
        ranking.setText(String.valueOf(score));

		if (score > 0) {
			ranking.setTextColor(Color.parseColor("#00ff00"));
		} else if (score < 0) {
			ranking.setTextColor(Color.parseColor("#ff0000"));
		}
    }
}
