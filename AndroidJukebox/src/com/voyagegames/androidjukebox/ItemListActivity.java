package com.voyagegames.androidjukebox;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.voyagegames.androidjukebox.data.Content;

public class ItemListActivity
		extends FragmentActivity
		implements ItemListFragment.Callbacks,
				ItemDetailFragment.OnRankingChangedListener {

    private boolean mTwoPane;
    private Context mContext;
    private MusicAsyncTask mTask;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_item_list);
        
        final Void[] emptyParams = null;
        
        mTask = new MusicAsyncTask(this);
        mTask.execute(emptyParams);

        if (findViewById(R.id.item_detail_container) != null) {
            mTwoPane = true;
            ((ItemListFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.item_list))
                    .setActivateOnItemClick(true);
        }
        
        final Button addButton = (Button)findViewById(R.id.item_list_add_button);
        addButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
	            startActivity(new Intent(mContext, ArtistListActivity.class));
			}
        
        });
    }

    @Override
	public void onBackPressed() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        
        builder.setMessage(R.string.prompt_to_exit)
               .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                   public void onClick(final DialogInterface dialog, final int id) {
	                   	mTask.releasePlayer();
	                   	finish();
                   }
               })
               .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                   public void onClick(final DialogInterface dialog, final int id) {
                       // no-op
                   }
               });

        builder.create();
        builder.show();
	}

	@Override
    public void onItemSelected(final String id) {
        if (mTwoPane) {
            final Bundle arguments = new Bundle();
            final ItemDetailFragment fragment = new ItemDetailFragment();
            
            arguments.putString(ItemDetailFragment.ARG_ITEM_ID, id);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();
        } else {
        	final Intent detailIntent = new Intent(this, ItemDetailActivity.class);
            detailIntent.putExtra(ItemDetailFragment.ARG_ITEM_ID, id);
            startActivity(detailIntent);
        }
    }

	@Override
	public void onRankingChanged() {
		onListUpdated();
		
		final ItemListFragment listFragment = (ItemListFragment)getSupportFragmentManager().findFragmentById(R.id.item_list);

        if (listFragment != null) {
            // We're in two-pane layout...
            // Call a method in the fragment to update its content
        	listFragment.updateAdapter();
        } else {
            // Otherwise, we're in the one-pane layout and must swap fragments...
        	final ItemListFragment newFragment = new ItemListFragment();
            newFragment.setArguments(new Bundle());

            // Replace whatever is in the view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            //
            // Allow state loss, per
            //    http://stackoverflow.com/questions/7469082/getting-exception-illegalstateexception-can-not-perform-this-action-after-onsa
            getSupportFragmentManager().beginTransaction()
            		.replace(R.id.item_list, newFragment)
            		.addToBackStack(null)
            		.commitAllowingStateLoss();
        }
	}

	@Override
	public void onListUpdated() {
		if (Content.ITEMS.size() > 0) {
			mTask.updateSong(Content.ITEMS.get(0));
		} else {
			mTask.stopPlayer();
		}
	}
	
}
