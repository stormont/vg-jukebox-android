package com.voyagegames.androidjukebox;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.voyagegames.androidjukebox.data.Content;

public class ItemListFragment extends ListFragment {
	
	public class NotifyReceiver extends BroadcastReceiver {
    
		@Override
        public void onReceive(final Context context, final Intent intent) {
			final int index = intent.getIntExtra(Content.FRAGMENT_NUM, Content.FragmentIndex.BAD_INDEX.ordinal());
			
			if (index != Content.FragmentIndex.ITEM_LIST.ordinal()) {
				return;
			}
			
			updateAdapter();
			mCallbacks.onListUpdated();
        }    
	
	}

    public interface Callbacks {

        public void onItemSelected(String id);
        public void onListUpdated();
        
    }

    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    private Callbacks mCallbacks = sDummyCallbacks;
    private int mActivatedPosition = ListView.INVALID_POSITION;
    private NotifyReceiver mReceiver;

    private static Callbacks sDummyCallbacks = new Callbacks() {
    	
        @Override
        public void onItemSelected(final String id) {
        	// no-op
        }

		@Override
		public void onListUpdated() {
			// no-op
		}
        
    };

	@Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceiver = new NotifyReceiver();
        getActivity().registerReceiver(mReceiver, new IntentFilter(NotifyReceiver.class.getName()));
        updateAdapter();
    }

	@Override
	public void onDestroy() {
    	getActivity().unregisterReceiver(mReceiver);
		super.onDestroy();
	}

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallbacks = (Callbacks) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement " + Callbacks.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);
        mCallbacks.onItemSelected(String.valueOf(Content.ITEMS.get(position).song.id));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    public void setActivateOnItemClick(boolean activateOnItemClick) {
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    public void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
    
    public void updateAdapter() {
        setListAdapter(new JukeboxItemAdapter(getActivity(),
                android.R.layout.simple_list_item_activated_1,
                android.R.id.text1,
                Content.ITEMS));
    }
}
