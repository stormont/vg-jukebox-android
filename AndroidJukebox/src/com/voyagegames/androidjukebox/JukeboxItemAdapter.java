package com.voyagegames.androidjukebox;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.voyagegames.androidjukebox.data.Content;
import com.voyagegames.androidjukebox.data.Content.JukeboxContent;

public class JukeboxItemAdapter extends ArrayAdapter<JukeboxContent> {
	
	private final Activity mActivity;

	public JukeboxItemAdapter(
			final Context context,
			final int resource,
			final int textViewResourceId,
			final List<JukeboxContent> objects) {
		super(context, resource, textViewResourceId, objects);
		mActivity = (Activity)context;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View row = mActivity.getLayoutInflater().inflate(R.layout.fragment_item_list, parent, false);
		final TextView artist = (TextView)row.findViewById(R.id.item_list_artist);
		final TextView title = (TextView)row.findViewById(R.id.item_list_title);
		final TextView ranking = (TextView)row.findViewById(R.id.item_list_ranking);
		final int score = Content.ITEMS.get(position).ranking.score();
		
		artist.setText(Content.ITEMS.get(position).data.artist);
		title.setText(Content.ITEMS.get(position).data.title);
		ranking.setText(String.valueOf(score));
		
		if (position == 0) {
			artist.setTextColor(Color.parseColor("#00ddff"));
			title.setTextColor(Color.parseColor("#00ddff"));
			ranking.setTextColor(Color.parseColor("#00ddff"));
		} else {
			if (score > 0) {
				ranking.setTextColor(Color.parseColor("#00ff00"));
			} else if (score < 0) {
				ranking.setTextColor(Color.parseColor("#ff0000"));
			}
		}
		
		return row;
	}

}
