package com.voyagegames.androidjukebox;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;

import com.voyagegames.androidjukebox.ItemDetailFragment.OnRankingChangedListener;
import com.voyagegames.androidjukebox.data.Content;
import com.voyagegames.androidjukebox.data.Content.JukeboxContent;
import com.voyagegames.core.android.media.MediaPlayerHelper;

public class MusicAsyncTask extends AsyncTask<Void, Void, Void> {
	
	private final MediaPlayerHelper mPlayer;
	private final Activity mListener;
	
	private JukeboxContent mSong;
	
	public MusicAsyncTask(final Activity parent) {
		mPlayer = new MediaPlayerHelper(parent);
		mListener = parent;
	}

	@Override
	protected Void doInBackground(final Void... params) {
		while (true) {
			// Spin here; update the song playing through updateSong()
			try {
				Thread.sleep(500);
			} catch (final InterruptedException e) {
				// no-op; ignore interruption
			}
		}
	}

	@Override
	protected void onCancelled() {
		mPlayer.release();
		super.onCancelled();
	}

	@Override
	protected void onPostExecute(final Void result) {
		mPlayer.release();
		super.onPostExecute(result);
	}
	
	public void updateSong(final JukeboxContent song) {
		synchronized (this) {
			if (mSong == null || song.data.id != mSong.data.id) {
				if (mPlayer.player().isPlaying()) {
					mSong.setSeek(mPlayer.player().getCurrentPosition());
				}
				
				mSong = song;
				mPlayer.play(Uri.parse(mSong.data.uri), mSong.seek());
			}
		}
		
		mPlayer.player().setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(final MediaPlayer mp) {
				Content.popItem();
				
				if (Content.ITEMS.size() > 0) {
					updateSong(Content.ITEMS.get(0));
				}
				
				mListener.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						((OnRankingChangedListener)mListener).onRankingChanged();
					}
				});
			}
		});
	}
	
	public void stopPlayer() {
		mPlayer.player().stop();
	}
	
	public void releasePlayer() {
		mPlayer.release();
	}
	
}
