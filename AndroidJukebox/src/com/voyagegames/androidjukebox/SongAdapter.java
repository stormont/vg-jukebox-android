package com.voyagegames.androidjukebox;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.voyagegames.core.android.audio.Song;

public class SongAdapter extends ArrayAdapter<Song> {
	
	private final Activity mActivity;

	public SongAdapter(final Context context, final int resource, final int textViewResourceId, final List<Song> objects) {
		super(context, resource, textViewResourceId, objects);
		mActivity = (Activity)context;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		final View row = mActivity.getLayoutInflater().inflate(R.layout.song_list_row, parent, false);
		final TextView title = (TextView)row.findViewById(R.id.song_list_title);
		
		title.setText(this.getItem(position).title);
		return row;
	}

}
