package com.voyagegames.androidjukebox.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.voyagegames.core.android.audio.Song;
import com.voyagegames.jukebox.interfaces.IRanking;
import com.voyagegames.jukebox.modules.Item;
import com.voyagegames.jukebox.modules.Jukebox;
import com.voyagegames.jukebox.modules.Ranking;

public class Content {
	
	public enum FragmentIndex {
		BAD_INDEX,
		ITEM_LIST,
		ITEM_DETAIL,
		ARTIST_LIST,
		ARTIST_DETAIL
	}

    public static class JukeboxContent extends Item<Song> {

        final public Song song;
        
        private int mSeek;

        public JukeboxContent(final Song content, final IRanking ranking) {
        	super(content, ranking);
        	this.song = content;
        	mSeek = 0;
        }
        
        public int seek() {
        	return mSeek;
        }
        
        public void setSeek(final int seek) {
        	mSeek = seek;
        }

        @Override
        public String toString() {
            return song.title;
        }
        
    }
    
    public final static String FRAGMENT_NUM = "FragmentNum";
    public final static List<JukeboxContent> ITEMS = new ArrayList<JukeboxContent>(); 
    public final static Map<String, JukeboxContent> ITEM_MAP = new HashMap<String, JukeboxContent>();
    private final static Jukebox<Song> JUKEBOX = new Jukebox<Song>();

    public static void addItem(final Song item) {
    	final JukeboxContent content = new JukeboxContent(item, new Ranking());
    	
    	synchronized(JUKEBOX) {
	    	JUKEBOX.enqueue(content);
	    	ITEMS.add(content);
	    	ITEM_MAP.put(String.valueOf(content.song.id), content);
    	}
    }
    
    public static JukeboxContent popItem() {
    	if (ITEMS.size() == 0) {
    		return null;
    	}
    	
    	synchronized(JUKEBOX) {
    		final JukeboxContent song = ITEMS.remove(0);
    		
    		ITEM_MAP.remove(String.valueOf(song.song.id));
    		JUKEBOX.pop();
        	return song;
    	}
    }
    
    public static void upVote(final JukeboxContent item) {
    	int position = findPosition(item);
    	
    	synchronized(JUKEBOX) {
    		JUKEBOX.upRank(position);
    		updateList();
    	}
    }
    
    public static void downVote(final JukeboxContent item) {
    	int position = findPosition(item);
    	
    	synchronized(JUKEBOX) {
    		JUKEBOX.downRank(position);
    		updateList();
    	}
    }
    
    private static int findPosition(final JukeboxContent item) {
    	int position = -1;
    	
    	for (int i = 0; i < ITEMS.size(); ++i) {
    		if (ITEMS.get(i).song.id == item.song.id) {
    			position = i;
    			break;
    		}
    	}
    	
    	return position;
    }
    
    private static void updateList() {
    	ITEMS.clear();
    	
    	for (Item<Song> i : JUKEBOX.items()) {
    		if (i.ranking.score() >= 0) {
    			ITEMS.add((JukeboxContent)i);
    		}
    	}
    }

}
